package com.example.antonija.v19_z1;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;

import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.ibBlueJay)
    ImageButton ibBlueJay;
    @BindView(R.id.ibPeacock)
    ImageButton ibPeacock;
    @BindView(R.id.ibDuck)
    ImageButton ibDuck;
    SoundPool _SoundPool;
    boolean _IsLoaded = false;
    private HashMap<Integer, Integer> _SoundMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadSounds();
        this.loadImages();
    }

    private void loadImages() {
        Picasso.with(getApplicationContext()).load(R.drawable.bluejay).fit().centerCrop().into(ibBlueJay);
        Picasso.with(getApplicationContext()).load(R.drawable.peacock).fit().centerCrop().into(ibPeacock);
        Picasso.with(getApplicationContext()).load(R.drawable.duck).fit().centerCrop().into(ibDuck);
    }

    private void loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this._SoundPool = new SoundPool.Builder().setMaxStreams(10).build();
        } else {
            this._SoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        }

        this._SoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                Log.d("Test", String.valueOf(sampleId));
                _IsLoaded = true;
            }
        });
        this._SoundMap.put(R.raw.bluejay, this._SoundPool.load(this, R.raw.bluejay, 1));
        this._SoundMap.put(R.raw.peacock, this._SoundPool.load(this, R.raw.peacock, 1));
        this._SoundMap.put(R.raw.duck, this._SoundPool.load(this, R.raw.duck, 1));
    }

     @OnClick(R.id.ibBlueJay)
     public void clickBlueJay()
     {
        playSound(R.raw.bluejay);
        Notification.Builder builder = new Notification.Builder(this);
        String title = "BlueJay";
        String text = "This is a blue jay sound.";
        long[] pattern = new long[] {500,500,500,500};
        Intent intent = new Intent(this, NotificationActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentTitle(title).setContentText(text).setAutoCancel(true).setSmallIcon(android.R.drawable.ic_dialog_info).setLights(Color.BLUE, 100, 100).setVibrate(pattern).setContentIntent(pendingIntent);
        Notification notification = builder.build();
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(10, notification);
        this.finish();
     }
    @OnClick(R.id.ibPeacock)
    public void clickPeacock()
    {
        playSound(R.raw.peacock);
        Notification.Builder builder = new Notification.Builder(this);
        String title = "Peacock";
        String text = "This is a peacock sound.";
        long[] pattern = new long[] {500,500,500,500};
        Intent intent = new Intent(this, NotificationActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentTitle(title).setContentText(text).setAutoCancel(true).setSmallIcon(android.R.drawable.ic_dialog_info).setLights(Color.GREEN, 100, 100).setVibrate(pattern).setContentIntent(pendingIntent);
        Notification notification = builder.build();
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(10, notification);
        this.finish();
    }
    @OnClick(R.id.ibDuck)
    public void clickDuck()
    {
        playSound(R.raw.duck);
        Notification.Builder builder = new Notification.Builder(this);
        String title = "Duck";
        String text = "This is a duck sound.";
        long[] pattern = new long[] {500,500,500,500};
        Intent intent = new Intent(this, NotificationActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentTitle(title).setContentText(text).setAutoCancel(true).setSmallIcon(android.R.drawable.ic_dialog_info).setLights(Color.YELLOW, 100, 100).setVibrate(pattern).setContentIntent(pendingIntent);
        Notification notification = builder.build();
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(10, notification);
        this.finish();
    }
    @OnClick({R.id.ibBlueJay, R.id.ibPeacock, R.id.ibDuck})
    public void clickButton(ImageButton ibutton) {
        switch (ibutton.getId()) {
            case R.id.ibBlueJay:
                this.clickBlueJay();
                this.finish();
                break;
            case R.id.ibPeacock:
                this.clickPeacock();
                this.finish();
                break;
            case R.id.ibDuck:
                this.clickDuck();
                this.finish();
                break;
        }
    }

    private void playSound(int resourceId) {
        int soundID = this._SoundMap.get(resourceId);
        this._SoundPool.play(soundID, 1,1,1,0,1F);
    }

}
